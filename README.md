# Backpack v1.0

A Personal Frontend Framework.

## Installation

Install with [Bower](https://bower.io) package manager.

```
$ bower install jjgrainger/backpack
```

## Usage

Backpack provides the [Sass](http://sass-lang.com/) source files for you to pick and choose the components you want to include into your project.

To include them use sass `@import`

```scss
@import 'path/to/backpack/sass/backpack'
```

## Notes

* Licensed under the [MIT License](#)
* Maintained under the [Semantic Versioning Guide](http://semver.org)

## Author

**Joe Grainger**

* [http://jjgrainger.co.uk](http://jjgrainger.co.uk)
* [http://twitter.com/jjgrainger](http://twitter.com/jjgrainger)
